//
//  NoticeTableViewCell.swift
//  ViperNewDemo
//
//  Created by Neshwa on 27/12/23.
//

import UIKit

class NoticeTableViewCell: UITableViewCell {
    
 
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var noticeTitle: UILabel!
    @IBOutlet weak var noticeDescription: UITextView!
    @IBOutlet weak var noticeURL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 10
        mainView.layer.borderColor = UIColor.black.cgColor
        mainView.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
