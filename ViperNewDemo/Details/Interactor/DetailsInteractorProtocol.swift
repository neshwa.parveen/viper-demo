//
//  DetailsInteractorProtocol.swift
//  ViperNewDemo
//
//  Created by Neshwa on 28/12/23.
//

import Foundation

protocol DetailsInteractorInput {
    func fetchDetails()
}

protocol DetailsInteractorOutput {
    
}
