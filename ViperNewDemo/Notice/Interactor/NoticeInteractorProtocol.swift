//
//  NoticeInteractorProtocol.swift
//  ViperNewDemo
//
//  Created by Neshwa on 22/12/23.
//

import Foundation

protocol NoticeInteractorInput {
    func fetchNotices()
}

protocol NoticeInteractorOutput {
    func noticesFetched(_ notice : Notice)
}
