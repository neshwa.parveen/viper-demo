//
//  NoticeInteractor.swift
//  ViperNewDemo
//
//  Created by Neshwa on 22/12/23.
//

import Foundation
import Alamofire

class NoticeInteractor : NoticeInteractorInput {
    
    let apiService : APIService = ApiServiceImplimentation()
    var output : NoticeInteractorOutput?
    
    func fetchNotices() {
        
        apiService.fetchPosts { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let notices):
                //print("notices : \(notices)")
                output?.noticesFetched(notices)
            case .failure(let error):
                print("eroor \(error)")
            }
        }
    }
    
    
}
