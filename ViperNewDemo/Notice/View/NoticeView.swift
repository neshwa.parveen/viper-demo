//
//  NoticeView.swift
//  ViperNewDemo
//
//  Created by Neshwa on 22/12/23.
//

import Foundation

protocol NoticeView : AnyObject {
    func displayTasks(_ tasks: [ArticleModel])
}

struct ArticleModel {
    let title: String
    let description: String?
    let url: String
    
}
