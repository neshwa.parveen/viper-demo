//
//  ViewController.swift
//  ViperNewDemo
//
//  Created by Neshwa on 22/12/23.
//

import UIKit

class NewsViewController: UIViewController {
    
    @IBOutlet weak var noticeTable: UITableView!
    var presenter: NoticePresenter!
    var articles : [ArticleModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        noticeTable.dataSource = self
        noticeTable.delegate = self
        noticeTable.register(UINib(nibName: "NoticeTableViewCell", bundle: nil), forCellReuseIdentifier: "NoticeTableViewCell")
        noticeTable.estimatedRowHeight = 100
        noticeTable.rowHeight = UITableView.automaticDimension
        presenter = NoticePresenter()
        presenter.view = self
        
        let interactor = NoticeInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        interactor.fetchNotices()
    }
}

extension NewsViewController : NoticeView {

    func displayTasks(_ tasks: [ArticleModel]) {
        articles = tasks
        noticeTable.reloadData()
    }
}

extension NewsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NoticeTableViewCell", for: indexPath) as? NoticeTableViewCell else { return UITableViewCell() }
    
        cell.noticeTitle.text = articles[indexPath.row].title
        cell.noticeDescription.text = articles[indexPath.row].description
        cell.noticeURL.text = articles[indexPath.row].url
        cell.selectionStyle = .none
        return cell
    }
}
