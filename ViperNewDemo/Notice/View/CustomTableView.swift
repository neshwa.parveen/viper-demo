//
//  CustomTableView.swift
//  ViperNewDemo
//
//  Created by Neshwa on 27/12/23.
//

import Foundation
import UIKit

class CustomTableView: UITableView {

    private let maxHeight = CGFloat(300)
    private let minHeight = CGFloat(100)

    override public func layoutSubviews() {
        super.layoutSubviews()
        if bounds.size != intrinsicContentSize {
            invalidateIntrinsicContentSize()
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        if contentSize.height > maxHeight {
            //return CGSize(width: contentSize.width, height: maxHeight)
            return CGSize(width: contentSize.width, height: contentSize.height)
        }
        else if contentSize.height < minHeight {
            return CGSize(width: contentSize.width, height: minHeight)
        }
        else {
            return contentSize
        }
    }

}
