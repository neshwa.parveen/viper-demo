//
//  NoticePresenter.swift
//  ViperNewDemo
//
//  Created by Neshwa on 22/12/23.
//

import Foundation

class NoticePresenter : NoticeInteractorOutput {
    
    var view : NoticeView?
    var interactor : NoticeInteractor?
    var notices : Notice?
    
    func noticesFetched(_ notice: Notice) {
        
        self.notices = notice
        let notices = notice.articles.map { notice in
            return ArticleModel(title: notice.title, description: notice.description, url: notice.url)
        }
        view?.displayTasks(notices)
    }
    
    //    func numberOfItems() -> Int {
    //        notices?.articles.count ?? 0
    //    }
    //
    //    func notice(for indexPath: IndexPath) -> ArticleModel? {
    //        let noticeForIndex = notices?.articles[indexPath.row]
    //        return ArticleModel(title: noticeForIndex?.title ?? "", description: noticeForIndex?.description ?? "", url: noticeForIndex?.url ?? "")
//}

    

}
