//
//  APIService.swift
//  ViperNewDemo
//
//  Created by Neshwa on 22/12/23.
//

import Foundation

protocol APIService {
    func fetchPosts(completion: @escaping (Result<Notice, Error>) -> Void)
}
