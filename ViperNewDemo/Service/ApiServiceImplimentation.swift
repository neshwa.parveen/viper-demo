//
//  ApiServiceImplimentation.swift
//  ViperNewDemo
//
//  Created by Neshwa on 22/12/23.
//

import Foundation
import Alamofire

class ApiServiceImplimentation : APIService {
    
    func fetchPosts(completion: @escaping (Result<Notice, Error>) -> Void) {
        AF.request("https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=11049b561eaf462e98160b5c1008f6ce")
            .validate()
            .responseDecodable(of: Notice.self) { response in
                switch response.result {
                case .success(let notices):
                    completion(.success(notices))
                    //print("posts \(notices)")
                case .failure(let error):
                    completion(.failure(error))
                    //print("Error \(error)")
                }
            }
    }
}
